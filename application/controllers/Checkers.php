<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkers extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		if($this->session->userdata('logged_in'))
		{
			
			
		}
		else
		{
			//If no session, redirect to login page
			$this->load->view('pages/login_view');
		}
	}

	function checkLogin()
	{
		$this->load->library('form_validation');

	    $this->form_validation->set_rules('username', 'username', 'trim|required');
	    $this->form_validation->set_rules('password', 'password', 'trim|required');

	    if ($this->form_validation->run() == FALSE)
		{
		    $this->load->view('pages/login_view');

		}
		else
		{
			$user = $this->input->post('username');
		    $pass = $this->input->post('password');
			
			$result = $this->User_model->login($user, $pass);


			if($result)
			{
				$sess_array = array();
				foreach($result as $row)
				{
					$sess_array = array(
					 'ID' => $row->ID,
					 'user_name' => $row->user_name,
					 'user_status' => $row->user_status
					);
					$this->session->set_userdata('logged_in', $sess_array);
				}
				redirect(base_url('/dashboard'));
			}
			else
			{
				$data['error'] = "Wrong username or password!";
				$this->load->view('pages/login_view', $data);
			}
		}
	}

	function newFirma()
	{
		$this->load->library('form_validation');

	    $this->form_validation->set_rules('vorname', 'vorname', 'trim|required');
	    $this->form_validation->set_rules('name', 'name', 'trim|required');

	    if ($this->form_validation->run() == FALSE)
		{
			$session_data = $this->session->userdata('logged_in');
       		$data['username'] = $session_data['user_name']; 

		    $this->load->view('include/header', $data);
		    $this->load->view('pages/register-e_view');
		    $this->load->view('include/footer');

		}
		else
		{
			$user = $this->input->post('username');
		    $pass = $this->input->post('password');
			
			$result = $this->User_model->login($user, $pass);


			if($result)
			{
				$sess_array = array();
				foreach($result as $row)
				{
					$sess_array = array(
					 'ID' => $row->ID,
					 'user_name' => $row->user_name,
					 'user_status' => $row->user_status
					);
					$this->session->set_userdata('logged_in', $sess_array);
				}
				redirect(base_url('/dashboard'));
			}
			else
			{
				$data['error'] = "Wrong username or password!";
				$this->load->view('pages/login_view', $data);
			}
		}
	}

	function addNewClient()
	{
		

			$user_type = $this->input->post('privatefirma');
			if($user_type == 'private')
			{
				$v_kp = $this->input->post('vergbarkeit');
			}else{
				$v_kp = $this->input->post('firmakontaktperson');
			}
			$vorname = $this->input->post('vorname');
		    $name = $this->input->post('name');
		    $strassenr = $this->input->post('strassenr');
		    $plz = $this->input->post('plz');
		    $ort = $this->input->post('ort');
		    $email = $this->input->post('email');
		    $tel = $this->input->post('tel');
		    $mobile = $this->input->post('mobile');
		    $schlussenr = $this->input->post('schlussenr');
		    $datumentgegennahme = $this->input->post('datumentgegennahme');

		    $n_vorname = $this->input->post('new_vorname');
	    	$n_name = $this->input->post('new_name');
	    	$n_strasse = $this->input->post('new_strassenr');
	    	$n_plz = $this->input->post('new_plz');
	    	$n_ort = $this->input->post('new_ort');
	    	$n_email = $this->input->post('new_email');
	    	$n_tel = $this->input->post('new_email');
		    


		    // check if adedd a new address form
		    $newaddress = $this->input->post('adresse');
		    if($newaddress == 'new')
		    {
		    	$newaddress = 1;
		    	
		    	

		    	$result = $this->User_model->privateKunden($user_type, $v_kp, $vorname, $name, $strassenr, $plz, $ort, $email, $tel, $mobile, $schlussenr, $datumentgegennahme, $newaddress, $n_vorname, $n_name, $n_strasse, $n_plz, $n_ort, $n_email, $n_tel);
		    	
		    }else{

		    	// insert the information into database
		    	$result = $this->User_model->privateKunden($user_type, $v_kp, $vorname, $name, $strassenr, $plz, $ort, $email, $tel, $mobile, $schlussenr, $datumentgegennahme, $newaddress);
		    }

			


			if($result)
			{
				
				redirect (base_url('/index.php/dashboard/success'));
			}
			else
			{
				redirect (base_url('/index.php/dashboard/error'));
			}
		
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect(base_url());
	}

}