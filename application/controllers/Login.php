<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  
  function index()
  {
      if($this->session->userdata('logged_in'))
      {
       $session_data = $this->session->userdata('logged_in');
       $data['username'] = $session_data['user_name'];
        
        $this->load->view('include/header', $data);
        $this->load->view('pages/dashboard_view', $data);
        $this->load->view('include/footer');
      }
      else
      {
        //If no session, redirect to login page
         $this->load->view('pages/login_view');
      }
 } 

 function dashboard($alert = '')
 {
    $data['success'] = false;
    if ($alert == 'success') {
      $data['success'] = true;
    }

    if($this->session->userdata('logged_in'))
    {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['user_name'];

     $this->load->view('include/header', $data);
     $this->load->view('pages/dashboard_view', $data);
     $this->load->view('include/footer', $data);
    }
    else
    {
      //If no session, redirect to login page
       $this->load->view('pages/login_view');
    }
    
 }
}
