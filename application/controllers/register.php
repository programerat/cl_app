<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

  
  function index()
  {
      if($this->session->userdata('logged_in'))
      {
       $session_data = $this->session->userdata('logged_in');
       $data['username'] = $session_data['user_name']; 
        
        $this->load->view('include/header', $data);
        $this->load->view('pages/register-e_view', $data);
        $this->load->view('include/footer'); 
      }
      else
      {
        //If no session, redirect to login page
         $this->load->view('pages/login_view');
      }
 } 

 function dashboard($check)
 {
    $data['success'] = false;

    if($this->session->userdata('logged_in'))
    {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['user_name'];

     if($check == 'success')
     {
      $data['success'] = true;
     }

     $this->load->view('include/header', $data);
     $this->load->view('pages/dashboard_view', $data);
     $this->load->view('include/footer', $data);
    }
    else
    {
      //If no session, redirect to login page
       $this->load->view('pages/login_view');
    }
    
 }


 // Logout function 
 function logout()
  {
    $this->session->unset_userdata('logged_in');
    session_destroy();
    redirect(base_url());
  }
}
