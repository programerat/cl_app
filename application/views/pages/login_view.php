<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		@import url(http://fonts.googleapis.com/css?family=Poiret+One);

		img { background-image: url('http://static.hdw.eweb4.com/media/wallpapers_1920x1200/abstract/1/1/gradient-blur-abstract-hd-wallpaper-1920x1200-2384.png');  }
		
		article{
			width: 35%;
			height: auto;
			background-color: #FFFFFF;
			margin: 0 auto ;
			text-align: center;
			padding: 20px;
			font-family: Helvetica;
			margin-top: 20%;
		}

		h2 { font-family: 'Poiret One', cursive; font-weight: bold; font-size: 28px; margin: 0;}

		hr{ border: 0; border-top: 1px dashed #6FAE44; height: 0px; margin: 20px 0;}

		.error {
			width: auto;

			color: #fff;
			padding: 5px 0;
			background-color: #EA2E49;
			margin: 10px;
		}

		form{
			background-color: #E3E3E3;
			height: auto;
			margin: 10px;
			padding: 30px;
			text-align: left;
		}

		form .form-input {
			width: 100%;
			height: 30px;
			border: 1px solid #E0E0E0;
			margin: 5px 0;
			font-size: 18px;
			padding: 0 5px;
		}

		form button {
			width: 100px;
			height: 40px;
			border: #E0E0E0;
			background-color: #6FAE44;
			color: #fff;
			margin: 10px 0;
			font-size: 18px;
		}

		form .fyp {
			float: right;
			text-decoration: none;
			color: #6FAE44;
			clear: both;
		}

	</style>
</head>
<body>
	<article>
		<h2>
			Login Page
		</h2>
		<hr>
		
			<?php echo validation_errors('<div class="error">', '</div>'); ?>
			<?php if (isset($error)) echo '<div class="error">'.$error.'</div>'; ?>
		
		<form method="post" action="<?php echo base_url('/index.php/Checkers/checkLogin/'); ?>" >
			<label for="Email">Email or Username</label>
			<input type="text" id="Email" name="username" class="form-input" >
			<label for="password">Password</label>
			<input type="password" id="password" name="password" class="form-input" >
			<a href="#" class="fyp">Furgot your password?</a>
			<button>Login</button>
		</form>
	</article>
</body>
</html>