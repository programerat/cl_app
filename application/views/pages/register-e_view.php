<div class="col-md-12">
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url(); ?>">Home</a></li>
		<li class="active">Kunde erfassen</li>
	</ol>
</div>


<div class="col-md-6 col-md-offset-3">
	<h3 id="type">Typ wählen</h3>
	<?php echo validation_errors(); ?>
	<!-- This is the first form for private kunden -->
	<form action="<?php echo base_url('/index.php/checkers/addNewClient/'); ?>" method="post" id="form">
	<div class="radio">
		<input type="radio" name="privatefirma" id="private" value="private">
		<label for="private"><span>Private kunden</span></label>
		<input type="radio" name="privatefirma" id="firma" value="firma">
		<label for="firma"><span>Firma</span></label>
	</div>

	<!-- This is the second form for the firma kontakt -->
	<div id="form1"></div>
	<!-- This is the radiobutton for asking the address -->

	<div class="radio" id="oldnew">
		<input type="radio" name="adresse" id="oldaddress" value="old" >
		<label for="oldaddress"><span>Kundenadresse </span></label>
		<input type="radio" name="adresse" id="newaddress" value="new">
		<label for="newaddress"><span>Abweichende</span></label>
	</div>
	<div id="newaddressform">
		<!-- This form will be shown if the user wants to type a new adress -->
		
	</div>
	
	<!-- There will be asked how the client want to recieve the anouncment email or mobile -->
	<!-- if neither of them is written then the warning prompt will be shown -->
	
	<div class="radio" id="janein">
		Auftragsausfuhrung werunscht?
		<input type="radio" name="optionsRadios" id="sendoption" value="ja" >
		<label for="sendoption"><span>Ja</span></label>
		<input type="radio" name="optionsRadios" id="sendoption1" value="nein" >
		<label for="sendoption1"><span>Nein</span></label>
	</div>
	<div id="error"></div>
	<div id="buttons">
		<button class="btn primary"><i class="fa fa-check"></i> Submit Form</button>
		<button class="btn default" type="reset" ><i class="fa fa-refresh"></i> Reset Form</button>
	</div>
	</form>
</div>