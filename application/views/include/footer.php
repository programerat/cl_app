</div>
<footer class="footer">
<div class="container">
  <p class="text-muted">Footer.</p>
  <p><i class="fa fa-user"> </i> Contact</p>
  <p>About Us</p>
</div>
</footer>
<script src="<?php echo base_url('/js/jquery.js'); ?>"></script>
<script src="<?php echo base_url('/js/functions.js');  ?>"></script>
<script src="<?php echo base_url('/js/popup.js'); ?>"></script>
<?php if(isset($success) && $success): ?>
	<script> showAlert('Success !', 'The changes has been successfuly saved !', null, null, 'success', 'bl'); </script>
<?php endif; ?>

</body>
</html>