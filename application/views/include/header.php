<!DOCTYPE html>
<html>
  <head>
    <title> Dashboard </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  
    <!-- alert style imported here -->
    <link rel="stylesheet" href="<?php echo base_url('/css/popup.css'); ?>">

    <link rel="stylesheet" href="../css/register-e.css">
    <link rel="stylesheet" href="<?php echo base_url('css/main_style.css'); ?>">
  </head>
  <body>
    <nav>
      <div class="container">
        <div class="col-md-4">
          <ul class="nav nav-pills">
            <li role="presentation" class="active"><a href="<?php echo base_url(); ?>">Home</a></li>
            <li role="presentation"><a href="#">About Us</a></li>
            <li role="presentation"><a href="#">Contact</a></li>
          </ul>
        </div>
        <div class="col-md-4 col-md-offset-4">
          <a href=" <?php echo base_url('/register/'); ?> ">
            <button class="btn btn-info"><i class="fa fa-plus-circle"></i> Kunde erfassen</button>
          </a>

          
          <span class="username"><i class="fa fa-user"> </i> <?php echo $username; ?> </span>
          <a href=" <?php echo base_url('/index.php/register/logout'); ?> ">
            <button class="btn btn-danger" > Log out</button>
          </a>
        </div>
      </div>
      
    </nav>
    <div class="container">