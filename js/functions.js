$(document).ready(function(){

	var fform, sform;
	var prevent_submit = false;

	// Hides all the forms if the javascript is enabled
	// $('#newaddressform').hide();
	$('#oldnew').hide();
	$('#janein').hide();
	$('#buttons').hide();

	var privatekunden = '<div id="privatekunden">';
		privatekunden += '<label for="">Vorname</label>';
		privatekunden += '<input class="form-control" type="text" name="vorname" required>';
		privatekunden += '<label for="">Name</label>';
		privatekunden += '<input class="form-control" type="text" name="name" required>';
		privatekunden += '<label for="">Strasse, Nr.</label>';
		privatekunden += '<input class="form-control" type="text" name="strassenr">';
		privatekunden += '<label for="">PLZ</label>';
		privatekunden += '<input class="form-control" type="text" name="plz">';
		privatekunden += '<label for="">Ort</label>';
		privatekunden += '<input class="form-control" type="text" name="ort">';
		privatekunden += '<label for="">E-Mail</label>';
		privatekunden += '<input class="form-control" type="email"name="email" required>';
		privatekunden += '<label for="">Telefon</label>';
		privatekunden += '<input class="form-control" type="text" name="tel" required>';
		privatekunden += '<label for="">Verfugbarkeit</label>';
		privatekunden += '<input class="form-control" type="text" name="vergbarkeit">';
		privatekunden += '<label for="">Mobile</label>';
		privatekunden += '<input class="form-control" type="text" name="mobile">';
		privatekunden += '<label for="">Schlussel Nr.</label>';
		privatekunden += '<input class="form-control" type="text" name="schlussenr">';
		privatekunden += '<label for="">Datum Entgegennahme Schlussel</label>';
		privatekunden += '<input class="form-control" type="date" name="datumentgegennahme">';
	privatekunden += '</div>';

	var firma_form = '<div id="firmaform">';
		firma_form += '<label for="fkp">Firma Kontakt Person</label>';
		firma_form += '<input class="form-control" id="fkp" type="text" name="firmakontaktperson" required>';
		firma_form += '<label for="">Vorname</label>';
		firma_form += '<input class="form-control" type="text" name="vorname" required>';
		firma_form += '<label for="">Name</label>';
		firma_form += '<input class="form-control" type="text" name="name" required>';
		firma_form += '<label for="">Strasse, Nr.</label>';
		firma_form += '<input class="form-control" type="text" name="strassenr">';
		firma_form += '<label for="">PLZ</label>';
		firma_form += '<input class="form-control" type="text" name="plz">';
		firma_form += '<label for="">Ort</label>';
		firma_form += '<input class="form-control" type="text" name="ort">';
		firma_form += '<label for="">E-Mail</label>';
		firma_form += '<input class="form-control" type="email"name="email" required>';
		firma_form += '<label for="">Telefon</label>';
		firma_form += '<input class="form-control" type="text" name="tel" required>';
		firma_form += '<label for="">Mobile</label>';
		firma_form += '<input class="form-control" type="text" name="mobile">';
		firma_form += '<label for="">Schlussel Nr.</label>';
		firma_form += '<input class="form-control" type="text" name="schlussenr">';
		firma_form += '<label for="">Datum Entgegennahme Schlussel</label>';
		firma_form += '<input class="form-control" type="date" name="datumentgegennahme">';
	privatekunden += '</div>';

	var newaddressform = '<label for="">Vorname</label>';
		newaddressform += '<input class="form-control" type="text" name="new_vorname" required>';
		newaddressform += '<label for="">Name</label>';
		newaddressform += '<input class="form-control" type="text" name="new_name" required>';
		newaddressform += '<label for="">Strasse, Nr.</label>';
		newaddressform += '<input class="form-control" type="text" name="new_strassenr">';
		newaddressform += '<label for="">PLZ</label>';
		newaddressform += '<input class="form-control" type="text" name="new_plz">';
		newaddressform += '<label for="">Ort</label>';
		newaddressform += '<input class="form-control" type="text" name="new_ort">';
		newaddressform += '<label for="">E-Mail</label>';
		newaddressform += '<input class="form-control" type="email" name="new_email" required>';
		newaddressform += '<label for="">Telefon</label>';
		newaddressform += '<input class="form-control" type="text" name="new_tel" required>';

	// is called when the radio button is clicked
	$('input[type=radio]').change(function(){
		var check = $(this).attr('id');
		
		if(check == 'private')
		{
			$('#type').hide();
			$('#oldnew').fadeIn();
			$('#form1').html(privatekunden);
			fform = 'private';
		}
		if(check == 'firma')
		{
			$('#type').hide();
			$('#oldnew').fadeIn();
			$('#form1').html(firma_form);
			fform = 'firma';
			
		}
		if (check == 'oldaddress') 
		{
			$('#newaddressform').html('');
			sform = false;
			$('#janein').fadeIn();
		}
		if (check == 'newaddress') 
		{

			$('#newaddressform').html(newaddressform);
			sform = true;
			$('#janein').fadeIn();
			
		}
		

		
	});
	// check if the email exists
	$('input[id=sendoption]').change(function(){
		$('#buttons').fadeIn();

		if(fform == 'private')
		{
			var email = $('#privatekunden input[name=email]').val();
			var tel = $('#privatekunden input[name=tel]').val();
			
			// var et = $(this).attr('value').val();
			// alert(et);
			

			if(sform)
			{
				var newemail = $('#newaddressform input[name=new_email]').val();
				var newtel = $('#newaddressform input[name=mew_tel]').val();

				if(!newemail && !newtel )
				{
					$('#error').html('<div class="alert alert-danger" role="alert">Schreiben die email odder Tel</div>');
				}
			}else {
				if(!email && !tel )
				{
					$('#error').html('<div class="alert alert-danger" role="alert">Schreiben die email odder Tel</div>');
				}
			}

		}

		if(fform == 'firma')
		{
			var email = $('#firmaform input[name=email]').val();
			var tel = $('#firmaform input[name=tel]').val();
			
			// var et = $(this).attr('value').val();
			// alert(et);
			if(sform)
			{
				var newemail = $('#newaddressform input[name=new_email]').val();
				var newtel = $('#newaddressform input[name=new_tel]').val();

				if(!newemail && !newtel )
				{
					$('#error').html('<div class="alert alert-danger" role="alert">Schreiben die email odder Tel</div>');
				}
			}else {
				if(!email && !tel )
				{
					$('#error').html('<div class="alert alert-danger" role="alert">Schreiben die email odder Tel</div>');
				}
			}
		}
	});

	$('input[id=sendoption1]').change(function(){
		$('#error').html('');
		$('#buttons').fadeIn();
		prevent_submit = true;
	});

	$( "form" ).submit(function( event ) {

		$('')


	  if (prevent_submit ) {
	    return;
	  }else{
	  	$('#error').html( "Please complete all the form !" ).show().fadeOut( 1000 );
	  	event.preventDefault();
	  }
	 
	  
	});

	 

});