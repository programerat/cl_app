// This is alert or popup element for showing in the web page

function showAlert(title, text, img, time, color, position )
{

	$("#alert").remove();
	if (position == null) { position = 'bl'; };
	if (color == null) { color = 'default'};

	var html = "<div id='alert' class='"+position+"'>";
		html += "<div class='alertheader "+color+"'>";
		html += "<div class='alertclose'>x</div>";
		if (img != null) { html += "<img src='"+img+"' width='30px' />"};
		html += "<h3>"+title+"</h3>";
		html += "</div>";
		html += "<div class='alertbody'>";
		
		html += "<p>"+text+"</p></div></div>";

	$('body').append(html);

	if(time == null)
	{
		$("#alert").fadeIn().delay(3000).fadeOut();
	}else{
		$("#alert").fadeIn().delay(time).fadeOut();
	}
	
}
